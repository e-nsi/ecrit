# 📡 Réseaux

## :fontawesome-solid-network-wired: 2022

- [Métropole, J1, Exercice 3](./Reseaux/22-ME1-ex3.md)
    - Représentations binaires
    - Protocoles de routage

- [Métropole, J2, Exercice 3](./Reseaux/22-ME2-ex3.md)
    - Protocoles de routage
