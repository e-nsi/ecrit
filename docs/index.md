# Épreuve écrite

Exercices d'entrainement à l'épreuve écrite, d'après les sujets du baccalauréat. Les sujets originaux sont disponibles dans leur intégralité dans l'état original ci-dessous.

:warning: Les liens vers les exercices sont des versions modifiées de manière plus ou moins importante.


## Sujets originaux et versions modifiées

### 2023

- [Agro Veto](./BDD/23-agro-veto.md) : Base de données

### 2022

#### QCM pour les élèves de première

- [Sujet original](./sujets/2022/G1SNSIN03334.pdf)
    - [Version dynamique](QCM/G1SNSIN03334.md) (légèrement modifiée et corrigée)

#### Asie J2

- [Sujet original](./sujets/2022/22-NSIJ2JA1.pdf)
    - [Exercice 1](./Archi/22-A2-ex1.md) : Architecture
        - Commandes Linux via Python
    - [Exercice 2]
    - [Exercice 3]
    - [Exercice 4]
    - [Exercice 5](./Prog/22-A2-ex5.md) : Programmation
        - Exécution de programmes, recherche et corrections de bugs

#### Métropole J2

- Jeudi 12 mai 2022, [sujet original](./sujets/2022/22-NSIJ2ME1.pdf)
    - [Exercice 1](./Algo/22-ME2-ex1.md) : Algorithmique
        - Insertion dans un ABR (Arbre Binaire de Recherche)
        - Nombre de valeurs d'un ABR supérieures à une valeur donnée
    - [Exercice 2](./Struct/22-ME2-ex2.md) : Structures de données
        - Jeu de la *poussette* sur une pile
    - [Exercice 3](./Reseaux/22-ME2-ex3.md) : Réseaux
        - Protocoles de routage    
    - [Exercice 4](./BDD/22-ME2-ex4.md) : Base de données
        - 2 tables sur la musique
    - [Exercice 5](./Prog/22-ME2-ex5.md) : Programmation
        - Labyrinthe en POO

#### Métropole J1

- Mercredi 11 mai 2022, [sujet original](./sujets/2022/22-NSIJ1ME1.pdf)
    - [Exercice 1](./Struct/22-ME1-ex1.md) : Structures de données
        - Expressions bien parenthésées
        - État de la pile d'accumulation
    - [Exercice 2](./BDD/22-ME1-ex2.md) : Base de données
        - 2 tables sur le cinéma
    - [Exercice 3](./Reseaux/22-ME1-ex3.md) : Réseaux
        - Représentations binaires
        - Protocoles de routage
    - [Exercice 4](./Algo/22-ME1-ex4.md) : Algorithmique
        - Somme des valeurs d'un arbre binaire
    - [Exercice 5](./Prog/22-ME1-ex5.md) : Programmation
        - Jeu de LaserGame en POO

#### Centres étrangers J2

- Jeudi 12 mai 2022, [sujet original](./sujets/2022/22-NSIJ2G11.pdf)
    - [Exercice 1](./Algo/22-G11-J2-ex1.md) : Algorithmique
        - Produit d'une chaine de caractères et d'un entier
    - [Exercice 2](./Prog/22-G11-J2-ex2.md) : Programmation
        - Dictionnaire de chiffrement
    - [Exercice 3](./BDD/22-G11-J2-ex3.md) : Base de données
        - Évaluations d'élèves par compétence
    - [Exercice 4](./Prog/22-G11-J2-ex4.md) : Programmation
        - Jeu de la bataille en POO
    - Exercice 5 : Réseaux
        - :warning: Notation CIDR

#### Centres étrangers J1

- Mercredi 11 mai 2022, [sujet original](./sujets/2022/22-NSIJ1G11%20-%20Sujet.pdf)
    - [Exercice 1](./Prog/22-G11-J1-ex1.md) : Programmation
        - Jour suivant au format `(jour, j, m, a)`
    - [Exercice 2](./Struct/22-G11-J1-ex2.md) : Structures de données
        - Passage automatique en caisse (File et POO)
    - [Exercice 3](./Struct/22-G11-J1-ex3.md) : Structures de données
        - Arborescence de répertoire avec dictionnaire
    - [Exercice 4](./BDD/22-G11-J1-ex4.md) : Bases de données
        - Mesures du réchauffement climatique
    - [Exercices 5](./Archi/22-G11-J1-ex5.md) : Architecture
        - Comparaison de deux nano-ordinateurs
        - Réseaux et protocole RIP et OSPF

#### Polynésie J2

- Pas de sujet.

#### Polynésie J1

- Mercredi 4 mai 2022, [sujet original](./sujets/2022/22-NSIJ1PO1.pdf)
    - [Exercice 1](./Prog/22-PO1-ex1.md) : Programmation
        - Fonctions mutuellement récursives
        - Création de texte
    - [Exercice 2](./Archi/22-PO1-ex2.md) : Architecture matérielle
        - Ordonnancement
        - Expressions booléennes
    - [Exercice 3](./BDD/22-PO1-ex3.md) : Base de données
        - Modèle relationnel
        - Langage SQL
    - [Exercice 4](./Struct/22-PO1-ex4.md) : Structures de données
        - Tri de piles
    - [Exercice 5](./Algo/22-PO1-ex5.md) : Algorithmique
        - Construction d'arbres binaires de taille et de hauteur donnée

### 2021

#### Métropole, septembre, J2

- [Sujet original](./sujets/2021/21-Sujet_sept2_NSIJ2ME3.pdf)

#### Centres étrangers, J2

- [Sujet original](./sujets/2021/21-NSIJ2G11%20Sujet%20Principal%20J2%20SN%20Centres%20%C3%A9trangers%20G1%20Sujet_compressed.pdf)

#### Centres étrangers, J1

- [Sujet original](./sujets/2021/21-NSIJ1G11%20Sujet%20Principal%20J1%20SN%20Centres%20%C3%A9trangers%20G1%20Sujet_compressed.pdf)
  - [Exercice 1](./Prog/21-G11-J1-ex1.md) : Programmation orientée objet

#### Amérique du Nord

- [Sujet original](./sujets/2021/spe-numerique-informatique-2021-amerique-nord-1-sujet-officiel.pdf)

#### Polynésie

- [Sujet original](./sujets/2021/spe-numerique-informatique-2021-polynesie-2-sujet-officiel.pdf)

#### Métropole, candidats libres, J2

- Mardi 8 juin 2021, [Sujet original](./sujets/2021/spe-numerique-informatique-2021-metro-cand-libre-2-sujet-officiel.pdf)
    - [Exercice 1](BDD/21-ME2-ex1.md) : Base de données
        - 3 tables sur un CDI
    - [Exercice 2](Archi/21-ME2-ex2.md) : Architecture matérielle
        - Ordonnancement de processus
    - [Exercice 3](Algo/21-ME2-ex3.md) : Algorithmique
        - Arbres binaires « bien construits »
    - [Exercice 4](Prog/21-ME2-ex4.md) : Programmation
        - Mélange de Fisher-Yates
    - [Exercice 5](Prog/21-ME2-ex5.md) : Programmation
        - Sous-séquence de somme maximale

#### Métropole, candidats libres, J1

- [Sujet original](./sujets/2021/spe-numerique-informatique-2021-metro-cand-libre-1-sujet-officiel.pdf)


#### Métropole, annulée, J2

- [Sujet original](./sujets/2021/Sujet%202%20metropole%20mars%202021.pdf)

#### Métropole, annulée, J1

- [Sujet original](./sujets/2021/Sujet%201%20metropole%20mars%202021.pdf)



#### Sujet 0

- [Sujet original](./sujets/2021/spe-numerique-informatique-2021-zero-1-sujet-officiel.pdf)

### 2018

- [Centrale 2018](./BDD/18-centrale.md) : Base de données

### 2016

- [Centrale 2016](./BDD/16-centrale.md) : Base de données
