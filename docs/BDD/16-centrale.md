---
title: Plan de vol et sécurité aérienne
author: Guillaume Conan
---

> D'après Centrale 2016


Ce problème s'intéresse à différents aspects relatifs à la sécurité aérienne et plus précisément au risque de collision entre deux appareils. Dans cet exercice, nous abordons l'enregistrement des plans de vol des différentes compagnies aériennes.

Afin d'éviter les collisions entre avions, les altitudes de vol en croisière sont normalisées. Dans la majorité des pays, les avions volent à une altitude multiple de $1000$ pieds (un pied vaut 30,48 cm) au-dessus de la surface isobare à 1013,25 hPa. L'espace aérien est ainsi découpé en tranches horizontales appelées niveaux de vol et désignées par les lettres "FL" (*flight level*) suivies de l'altitude en centaines de pieds : "FL310" désigne une altitude de croisière de $31\,000$ pieds au-dessus de la surface isobare de référence. 


Eurocontrol est l'organisation européenne chargée de la navigation aérienne, elle gère plusieurs dizaines de milliers de vols par jour. Toute compagnie qui souhaite faire traverser le ciel européen à un de ses avions doit soumettre à cet organisme un plan de vol comprenant un certain nombre d'informations : trajet, heure de départ, niveau de vol souhaité, etc. Muni de ces informations, Eurocontrol peut prévoir les secteurs aériens qui vont être surchargés et prendre des mesures en conséquence pour les désengorger : retard au décollage, modification de la route à suivre, etc.


Nous modélisons (de manière très simplifiée) les plans de vol gérés par Eurocontrol sous la forme d'une base de données comportant deux tables : 


- la table **`vol`** qui répertorie les plans de vol déposés par les compagnies aériennes ; elle contient les colonnes
  
     - `id_vol` : numéro du vol (chaine de caractères) ;
     - `depart` : code de l'aéroport de départ (chaine de caractères) ;
     - `arrivee` : code de l'aéroport d'arrivée (chaine de caractères) ; 
     - `jour` : jour du vol (de type date, affiché au format `aaaa-mm-jj`) ;
     - `heure` : heure de décollage souhaitée (de type time, affiché au format `hh:mi`) ;
     - `niveau` : niveau de vol souhaité (entier).

|`id_vol`|`depart`     |`arrivee`     |  `jour`          |`heure`       |`niveau`     |
|--------|-----|-----|------------|-------|------:|
| AF1204 | CDG | FCO | 2016-05-02 | 07:35 | $300$ |
| AF1205 | FCO | CDG | 2016-05-02 | 10:25 | $300$ |
| AF1504 | CDG | FCO | 2016-05-02 | 10:05 | $310$ |
| AF1505 | FCO | CDG | 2016-05-02 | 13:00 | $310$ |


- la table **`aeroport`** qui répertorie les aéroports européens ; elle contient les colonnes
  
     - `id_aero` : code de l'aéroport (chaine de caractères) ;
     - `ville` : principale ville desservie (chaine de caractères) ;
     - `pays` : pays dans lequel se situe l'aéroport
  (chaine de caractères).
  
  
| `id_aero` | `ville`   | `pays` |
|-----------|-----------|--------|
| CDG       | Paris     | France |
| ORY       | Paris     | France |
| MRS       | Marseille | France |
| FCO       | Rome      | Italie |


Les types SQL `date` et `time` permettent de mémoriser respectivement un jour du calendrier grégorien et une heure du jour. Deux valeurs de type `date` ou de type `time` peuvent être comparées avec les opérateurs habituels (`=`, `<`, `<=`, etc.). La comparaison s'effectue suivant l'ordre chronologique. Ces valeurs peuvent également être comparées à une chaine de caractères correspondant à leur représentation externe (`'aaaa-mm-jj'` ou `'hh:mi'`).


1.  Écrire une requête SQL qui fournit le nombre de vols qui doivent décoller dans la journée du 2 mai 2016 avant midi.
1. Écrire une requête SQL qui fournit la liste des numéros de vols au départ d'un aéroport desservant Paris le 2 mai 2016.
1. Que fait la requête suivante ?
    ```sql
    SELECT id_vol
    FROM vol
        JOIN aeroport AS d ON d.id_aero = depart
        JOIN aeroport AS a ON a.id_aero = arrivee
    WHERE
        d.pays = 'France' AND
        a.pays = 'France' AND
        jour = '2016-05-02'
    ```
1.  Certains vols peuvent engendrer des conflits potentiels : c'est par exemple le cas lorsque deux avions suivent un même trajet, en sens inverse, le même jour et à un même niveau. Écrire une requête SQL qui fournit la liste des couples `(Id_vol_1 , Id_vol_2)` des identifiants des vols dans cette situation. 

