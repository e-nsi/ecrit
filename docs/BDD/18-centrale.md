---
title: Simulation du gaz parfait
author: Guillaume Conan
---

> D'après Centrale 2018


La théorie cinétique des gaz vise à expliquer le comportement macroscopique d'un gaz à partir des mouvements des particules qui le composent.  
Depuis la naissance de l'informatique, de nombreuses simulations numériques ont permis de retrouver les lois de comportement de différents modèles de gaz comme celui du gaz parfait.

Ce sujet s'intéresse à un gaz parfait monoatomique. Nous considèrerons que le gaz étudié est constitué de $N$ particules sphériques, toutes identiques, de masse $m$ et de rayon $R$, confinées dans un récipient rigide. Les simulations seront réalisées dans un espace à une, deux ou trois dimensions ; le récipient contenant le gaz sera, suivant le cas, un segment de longueur $L$, un carré de côté $L$ ou un cube d'arête $L$.

Dans le modèle du gaz parfait, les particules ne subissent aucune force (leur poids est négligé) ni aucune autre action à distance.  
Elles n'interagissent que par l'intermédiaire de chocs, avec une autre particule ou avec la paroi du récipient. Ces chocs sont toujours élastiques, c'est-à-dire que l'énergie cinétique totale est conservée. 

On dispose d'une fonction de simulation pour laquelle toutes les particules ne sont plus nécessairement identiques. Cette fonction enregistre ses résultats dans une base de données dont la structure est donnée :


```mermaid
classDiagram

    class SIMULATION{
        integer  SI_NUM
        datetime SI_DEB
        float SI_DUR
        integer SI_DIM
        float SI_L
    }
    
    class REBOND{
        integer SI_NUM
        integer RE_NUM
        integer PA_NUM
        float RE_T
        integer RE_DIR
        float RE_VIT
        float RE_P
        }
        
    class PARTICULE{
        integer PA_NUM
        varchar PA_NOM
        float PA_M
        float PA_R
    }
```


Cette base comporte les trois tables suivantes :

-   la table **`SIMULATION`** donne les caractéristiques de chaque simulation effectuée. Elle contient les colonnes

    -   `SI_NUM` numéro d'ordre de la simulation (clé primaire)
    -   `SI_DEB` date et heure du lancement du programme de simulation
    -   `SI_DUR` durée (en secondes) de la simulation (il ne s'agit pas du temps d'exécution du programme, mais du temps simulé)
    -   `SI_DIM` nombre de dimensions de l'espace de simulation
    -   `SI_N` nombre de particules pour cette simulation
    -   `SI_L` (en mètres) taille du récipient utilisé pour la simulation

-   la table **`PARTICULE`** des types de particules considérées. Elle contient les colonnes
    -   `PA_NUM` numéro (entier) identifiant le type de particule (clé primaire)
    -   `PA_NOM` nom de ce type de particule
    -   `PA_M` masse de la particule (en grammes)
    - `PA_R` rayon (en mètres) de la particule

-   la table **`REBOND`**, de clé primaire `(SI_NUM, RE_NUM)`, liste les chocs des particules avec les parois du récipient. Elle contient les colonnes

    -  `SI_NUM` numéro d'ordre de la simulation ayant généré ce rebond
    -  `RE_NUM` numéro d'ordre du rebond au sein de cette simulation
    -  `PA_NUM` numéro du type de particule concernée par ce rebond
    -  `RE_T` temps de simulation (en secondes) auquel ce rebond est arrivé
    -  `RE_DIR` paroi concernée : entier non nul de l'intervalle `[-SI_DIM, SI_DIM]` donnant la direction de la normale à la paroi. Ainsi $-2$ désigne la paroi située en $y=0$ alors que 1 désigne la paroi située en $x=L$
    -  `RE_VIT` norme de la vitesse de la particule qui rebondit (en $\textrm{m}\cdot \textrm{s}^{-1}$)
    -  `RE_VP` valeur absolue de la composante de la vitesse normale à la paroi (en $\textrm{m}\cdot\textrm{s}$)


Questions :

1.  Écrire une requête SQL qui donne le nombre de simulations effectuées pour chaque nombre de dimensions de l'espace de simulation.

2. Écrire une requête SQL qui donne, pour chaque simulation, le nombre de rebonds enregistrés et la vitesse moyenne des particules qui frappent une paroi.

3. Écrire une requête SQL qui, pour une simulation $n$ donnée, calcule, pour chaque paroi, la variation de quantité de mouvement due aux chocs des particules sur cette paroi tout au long de la simulation.  
On se rappellera que lors du rebond d'une particule sur une paroi la composante de sa vitesse normale à la paroi est inversée, ce qui correspond à une variation de quantité de mouvement de $2m|v_\bot|$ où $m$ désigne la masse de la particule et $v_\bot$ la composante de sa vitesse normale à la paroi.

