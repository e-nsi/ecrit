# ⌨️ Programmation

## :fontawesome-solid-code: 2022

- [Asie, J2, Exercice 5](./Prog/22-A2-ex5.md)
    - Exécution de programmes, recherche et corrections de bugs

- [Métropole, J1, Exercice 5](./Prog/22-ME1-ex5.md)
    - Jeu de LaserGame en POO

- [Métropole, J2, Exercice 5](./Prog/22-ME2-ex5.md)
    - Labyrinthe en POO

- [Centres étrangers, J2, Exercice 2](./Prog/22-G11-J2-ex2.md)
    - Dictionnaire de chiffrement

- [Centres étrangers, J2, Exercice 4](./Prog/22-G11-J2-ex4.md)
    - Jeu de la bataille en POO

- [Centres étrangers, J1, Exercice 1](./Prog/22-G11-J1-ex1.md)
    - Jour suivant au format `(jour, j, m, a)`

- [Polynésie, J1, Exercice 1](./Prog/22-PO1-ex1.md)
    - Fonctions mutuellement récursives
    - Création de texte

## :fontawesome-solid-code: 2021

- [Métropole, Candidats, libres, J2, Exercice 4](./Prog/21-ME2-ex4.md)
    - Mélange de Fisher-Yates

- [Centres étrangers, J1, Exercice 1](./Prog/21-G11-J1-ex1.md)
    - Chiffrement de César en POO

- [Métropole, Candidats, libres, J2, Exercice 5](./Prog/21-ME2-ex5.md)
    - Sous-séquence de somme maximale
