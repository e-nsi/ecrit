# 🗄️ Structures de données

## :fontawesome-solid-coins: 2022


- [Métropole, J1, Exercice 1](./Struct/22-ME1-ex1.md)
    - Piles et files.

- [Métropole, J2, Exercice 2](./Struct/22-ME2-ex2.md)
    - Piles et files.

- [Centres étrangers, J1, Exercice 2](./Struct/22-G11-J1-ex2.md)
    - Passage automatique en caisse (File et POO)

- [Centres étrangers, J1, Exercice 3](./Struct/22-G11-J1-ex3.md)
    - Arborescence de répertoire avec dictionnaire

- [Polynésie, J1, Exercice 4](./Struct/22-PO1-ex4.md)
    - Tri de piles.
