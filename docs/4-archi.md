# 🏗️ Architecture

## :fontawesome-solid-microchip: 2022

- [Asie, J2, Exercice 1](./Archi/22-A2-ex1.md)
    - Commandes Linux via Python

- [Centres étrangers, J1, Exercices 5](./Archi/22-G11-J1-ex5.md)
    - Comparaison de deux nano-ordinateurs
    - Réseaux et protocole RIP et OSPF

- [Polynésie, J1, Exercice 2](./Archi/22-PO1-ex2.md)
    - Ordonnancement
    - Expressions booléennes

## :fontawesome-solid-microchip: 2021

- [Métropole, Candidats libres, J2, Exercice 2](./Archi/21-ME2-ex2.md)
    - Ordonnancement de processus
