---
title: Routeurs et protocole OSPF
author: Franck CHAMBON
reviews: Nicolas Revéret, Romain Janvier
---

> D'après 2022, Métropole, J1, Ex. 3

**1.** Une adresse IPv4 est représentée sous la forme de 4 nombres entiers positifs séparés par des points. Chacun de ces 4 entiers peut être représenté sur un octet.

**1.a.** Donner en écriture décimale l'adresse IPv4 correspondant à l'écriture binaire : `11000000.10101000.10000000.10000011`

??? done "Réponse"
    $1 + 2 + 128 = 131$, ainsi l'adresse est `192.168.128.131`.


**1.b** Tous les ordinateurs du réseau A ont une adresse IPv4 de la forme : `192.168.128.___`, où seul le dernier octet (représenté par `___` ) diffère.

Donner le nombre d'adresses différentes possibles du réseau A.

??? done "Réponse"
    Sur les **256** adresses possibles avec 1 octet on trouvera :

    - la valeur 0 qui est réservée pour l'adresse IP du réseau ;
    - les valeurs 1 à 254 qui peuvent être utilisées pour les adresses des hôtes dans le réseau ;
    - la valeur 255 qui est réservée pour l'adresse de diffusion du réseau.


**2.** On rappelle que le protocole RIP cherche à minimiser le nombre de routeurs traversés (qui correspond à la métrique). On donne les tables de routage d'un réseau informatique composé de 5 routeurs (appelés A, B, C, D et E), chacun associé directement à un réseau du même nom, obtenues avec le protocole RIP :

=== "Routeur A"

    | Destination | Métrique |
    |---|---|
    | A | 0 |
    | B | 1 |
    | C | 1 |
    | D | 1 |
    | E | 2 |
    
=== "Routeur B"

    | Destination | Métrique |
    |---|---|
    | A | 1 |
    | B | 0 |
    | C | 2 |
    | D | 1 |
    | E | 2 |
    
=== "Routeur C"

    | Destination | Métrique |
    |---|---|
    | A | 1 |
    | B | 2 |
    | C | 0 |
    | D | 1 |
    | E | 2 |
    
    
=== "Routeur D"

    | Destination | Métrique |
    |---|---|
    | A | 1 |
    | B | 1 |
    | C | 1 |
    | D | 0 |
    | E | 1 |
    
    
=== "Routeur E"

    | Destination | Métrique |
    |---|---|
    | A | 2 |
    | B | 2 |
    | C | 2 |
    | D | 1 |
    | E | 0 |
    


**2.a.** Donner la liste des routeurs avec lesquels le routeur A est directement relié.

??? done "Réponse"
    Le routeur A est directement relié aux routeurs **B, C et D**, en effet la valeur de la métrique est de 1 pour ces destinations dans la table de routage du routeur A.

**2.b.** Représenter graphiquement et de manière sommaire les 5 routeurs ainsi que les liaisons existantes entre ceux-ci.

??? done "Réponse"

    ```mermaid
    flowchart LR
        a[(A)] --- b[(B)]
        a --- c[(C)]
        a --- d[(D)]
        b --- d[(D)]
        c --- d[(D)]
        d --- e[(E)]
    ```

**3.** Le protocole OSPF est un protocole de routage qui cherche à minimiser la somme des métriques des liaisons entre routeurs.  
Dans le protocole de routage OSPF le débit des liaisons entre routeurs agit sur la métrique via la relation : $\text{métrique} = \dfrac{10^8}{\text{débit}}$ dans laquelle le débit est
exprimé en bit par seconde ($\text{bps}$).

On rappelle qu'un $\text{kbps}$ est égal à $10^3~\text{bps}$ et qu'un $\text{Mbps}$ est égal à $10^6~\text{bps}$.

Recopier sur votre copie et compléter le tableau suivant :

|   |   |   |   |   |
|---|---|---|---|---|
| Débit              | $100~\text{kbps}$ | $500~\text{kbps}$ | ...  |  $100~\text{Mbps}$ |
| Métrique associée  | $1000$            | ...               | $10$ |  $1$               |


??? done "Réponse"
    Les deux lignes sont inversement proportionnelles.

    |   |   |   |   |   |
    |---|---|---|---|---|
    | Débit              | $100~\text{kbps}$ | $500~\text{kbps}$ | $10~\text{Mbps}$  |  $100~\text{Mbps}$ |
    | Métrique associée  | $1000$            | $200$             | $10$ |  $1$               |

**4.** Voici la représentation d'un réseau et la table de routage incomplète du routeur F obtenue avec le protocole OSPF :

![](./images/reseau.svg){ .autolight }

!!! abstract "Routeur F"

    | Destination | Métrique |
    |-------------|----------|
    | F | 0 |
    | G | 8 |
    | H | 5 |
    | I |   |
    | J |   |
    | K |   |
    | L |   |


Les nombres présents sur les liaisons représentent les coûts des routes avec le
protocole OSPF.

**4.a.** Indiquer le chemin emprunté par un message d'un ordinateur du réseau F à destination d'un ordinateur du réseau I. Justifier votre réponse.

??? done "Réponse"
    Le message sera acheminé du réseau F vers le réseau I en passant successivement par les routeurs **H, J et K**. En effet, avec ce trajet, le coût sera égal à **5 + 1 + 2 + 5** soit **13**. Tout autre trajet aura un coût plus élevé.


**4.b.** Recopier et compléter la table de routage du routeur F.

??? done "Réponse"

    | Destination   | Métrique  |
    |:-------------:|:---------:|
    | F             | 0         |
    | G             | 8         |
    | H             | 5         |
    | I             | **13**    |
    | J             | **6**     |
    | K             | **8**     |
    | L             | **11**    |


**4.c.** Citer une unique panne (sur une liaison ou un routeur) qui suffirait à ce que toutes les données des échanges de tout autre réseau à destination du réseau F transitent par le routeur G. Expliquer en détail votre réponse.

??? done "Réponse"
    Une panne de la liaison **F-H**.

    En considérant le routeur I : la liaison est directe avec F, mais pour un coût de 20. Or de I, en passant par les routeurs K, J et G, le coût sera seulement de 19. De fait les routeurs K et J privilégieront également le routeur G. Le routeur J deviendra aussi le routeur le plus économique pour les routeurs H et L.

    :warning: Une panne du **routeur H** n'est pas une réponse acceptable. D'après l'énoncé, tout réseau autre que F doit joindre ce dernier en passant par G.

